# TEBF - Test Engineer's Best Friend

## Warmup
**Anyone willing to take things seriously with Smart Contracts (many zeros) should at least glance over _Clarity_. Then**
- you know tests are of utmost importance in this context, find the basics in the **Clarity of Mind** book
- you'll play and juggle with SC behaviour to program your thing à la artist on a canvas
- it's the tough job of the Test Engineer to **actually** write the Clarinet tests in TypeScript and "fill the potholes"[^1] you keep stepping into; it's almost like you're aiming for them, btw :)

**Given there's a big % of tests mathching these conditions**
- a Clarinet test is a set of actions taken in succession like: method calls, transatcions, block mining
- checking an action requires a few asserts independent of anything else

then the picture naturally triggers words like "serialization", "linearity" and "functional" to the **trully** lazy person not wanting to write any tests or TypeScript :)

**After adding the last piece**
- knowledge of langs like Elm and ReScript that compile to JavaScript

off you jump on the search engines to rabbithole "TypeScript compilation", right? as anyone would.

**The Goal**
- give Test Engineers a lever to greatky amplify productivity allowing them to automate creation of a much larger set of tests in both quantity, quality and detail
- all inputs, outputs and operation would be along the line of Lists and function application
- all information has to be human readable to create a program you can trust in the most critical use cases
- the program is general enough to receive inputs from anywhere, a black box not depending on anything else

**The find is Derw alpha**
- a functional language heavily inspired by Elm borrowing its syntax which compiles to TypeScript
- an early version requiring toying around with, but one that works.
- a joy to find this piece work and thank you Noah whoever you are :)

**The Plan**
- use Derw to build a backend **Engine** that generates Clarinet TypeScript tests automatically
- allow variations on a test, arrangements of test actions and generation of complex scenarios via the functional way
- isolate the Engine as much as possible, use only a DB for communication thus presenting it as a black-box
- allow input in a very clear and human usable way from an external, unrelated and general frontend side
    - a Test Engineer describes what it is to be varied, combined, params, batches, structure, etc.
    - the frontend packages the test data, includes commands and writes to DB
    - the Engine triggers on the DB, decodes commands and allows altering running params
- the Engine output is clear-cut: correct TypeScript test files written to the DB
- allow output like Engine status and any usual stats in this context
- the DB allows monitoring, charting, archiving, etc.

Needless to say I come on the scene with the perfect record of experience, skill and knowledge firmly backing this all up, giving confidence and radiating potential
- no Smart Contract hands-on experience (= "xp")
- 1 day Haskell xp
- 1 day JS xp
- didn't like tests
- seeing the value in functional programming, finally
- seeing the value in Clarity, by chance
- an open mind

## Technical warmup
**Having**
- action = a method call or on-chain operation
- scenario = an ordering of a set of actions tested independently from anything else
- all actions and scenarios are accompanied by their own standalone asserts & checks
- Clarinet's test functions are independent (they work on separate simulated Chains), they form the main List
- -> = how it could be wrapped/approached functionally

**Then the test writing I'm targetting is at least any of** (more could be added, these deliver the idea)
- has many items to be checked by the same func like many contracts, methods, addresses -> a test func applied on a List
- has linear logic between test functions like a change of an action -> change applied on the main List
- rearranges actions in scenarios -> generate scenario List
- patterns/templates are repeatedly found in test funcs -> List of template funcs used to create List of final funcs

**Achieving the targets outlined above w/ these Lists of everything one must be able to either/or using Derw**
1. manipulate functions at a language level and drive compilation
2. define broad functions working on complex objects eg. those describing scenarios
3. generate Derw files filling Lists & creating funcs and use scripting for the final touches

**Current reality**
- [ ] have advanced function-level features
- [x] support for custom types in calls and function application
- [x] no problem compiling to functions accessing the filesystem, r/w/a to files isn't an issue

**Decision**
- use 2. & 3. for the moment
- keep an eye on proper ballance since Derw generates some TS boilerplate


## Operation
**Given**
- the work = generate TypeScript test code and add it to the test structure

**Input from the Test Engineer as human readable DB documents**
- commands describing new work for the Engine like create or edit tests
- practical information req. for each new work item just added
- allows for changing working Engine params

**Engine operation**
- trigger from DB by user's new commands document
- call to decoding routine that unwraps structure and fans out calls:
    - use the data to fill/create appropriate Lists, objects, Derw files, etc.
    - set running params, emit stats/logs
- allow user to check/edit relevant files like the Derw auto-generated ones
- compilation to TypeScript, run final scripts, output to DB (a file, an archived directory structure, a hash of the above if made available elsewhere)

**Engine output**
- correct test files or code in the expected location
- logs/live stats detailing its operation

## Ambition
**Given**
- the Engine starts with Clarinet but it's a black-box
- we're allready deep in functional, why not generalize all fixed/static data?

**Then have a general way of specifying _everything_**
- from base types to function body in Derw, all practical text that builds up everything
- logic must be generalized as well, some serious thought needs to be put in
- would be like plugins for specific frontends and it feels for the moment at the edge of the possible
- which means it is possible and of course it will be functional :)
- don't lose track, must **balance** between total effort **now** and effort **then** for this generalization

## Actual woodwork

Basic Derw type declaration
```elm
type alias CallArgs = { -- arguments for a Tx contract call
    contract:   string,
    method:     string,
    args:       Array<any>,
    sender:     string
}
```
generated TS output
```typescript
type CallArgs = {
    contract: string;
    method: string;
    args: Array<any>;
    sender: string;
}
```

List of contract call arguments
```elm
aCCList00 : List CallArgs -- valid contract names
aCCList00 = [{
    contract: "counter",
    method: "count-up",
    args: [],
    sender: ""
}]
--
aCCList01 : List CallArgs -- erroneous contract names
aCCList01 = [{
    contract: "counter000err",
    method: "count-up",
    args: [],
    sender: ""
}]
```
generated TS
```typescript
const aCCList00: CallArgs[] = [{
    contract: "counter",
    method: "count-up",
    args: [],
    sender: ""
}];

const aCCList01: CallArgs[] = [{
    contract: "counter000err",
    method: "count-up",
    args: [],
    sender: ""
}];
```

**On these examples** of basic translation it's normal to want actual Derw code generation of the aCCList01, 02, etc. Lists:
- user fills numerous Lists each detailing contract names, their methods & arguments, etc.
- frontend structures documents and writes to DB
- (backend) Engine reads DB and extracts all relevant data, like the CallArgs objects
- generate/append Derw files with this data looking just like the above code (w/ more entries)


**Add at runtime**
```elm
addToList : CallArgs -> List CallArgs -> List CallArgs
addToList a l =
    l.concat a
```
generated TS
```typescript
function addToList(a: CallArgs, l: CallArgs[]): CallArgs[] {
    return l.concat(a);
}
```

**Enough talk!** A Clarinet test
```elm
testf00 : Chain -> Map string Account -> void
testf00 chain m =
    let
        z : List CallArgs
        = addToList aCCList01[0] aCCList00
        a0 : void
        = assertEquals z.length 2
    in
        void[]
```
generated TS
```typescript
function testf00(chain: Chain, m: Map<string, Account>): void {
    const z: CallArgs[] = addToList(aCCList01[0], aCCList00);
    const a0: void = assertEquals(z.length, 2);
    return void[];
}
```
z is the new list of length 2, we assert and test passes.

**More mundane tasks**
- mutate a value from 1 CallArgs item = generate a new CallArgs item
- call the mutate function on a List = change this key for all its elements
```elm
-- change the "sender" key, return new
mutSender : CallArgs -> string -> CallArgs
mutSender a x = {
    contract: a.contract,
    method: a.method,
    args: a.args,
    sender: x
}

-- change "sender" to input string for the whole input List
fillSender : string -> List CallArgs -> List CallArgs
fillSender s argsList =
    argsList.map \a -> mutSender a s
```
and their TS code
```typescript
function mutSender(a: CallArgs, x: string): CallArgs {
    return {
        contract: a.contract,
        method: a.method,
        args: a.args,
        sender: x
        };
}

function fillSender(s: string, argsList: CallArgs[]): CallArgs[] {
    return argsList.map(function(a: any) {
        return mutSender(a, s);
    });
}
```

**Back to Clarinet**, how do we issue a Tx call?
```elm
-- issue Tx.contractCall with CallArgs from list
fillTxs : List CallArgs -> List Tx
fillTxs l =
    l.map \a -> Tx.contractCall a.contract a.method a.args a.sender
```
generated TS, like clockwork
```typescript
function fillTxs(l: CallArgs[]): Tx[] {
    return l.map(function(a: any) {
        return Tx.contractCall(a.contract, a.method, a.args, a.sender);
    });
}
```

**Mundane but helpful** to test if the key exists, asserting and also playing around with types
```elm
-- helper functions
mapgetAcc : Map string Account -> string -> Account
mapgetAcc m s =
    let
        a0 : void
        = assertEquals (m.has s) true
        x : any
        = m.get s
    in
        x
```
generated TS
```typescript
function mapgetAcc(m: Map<string, Account>, s: string): Account {
    const a0: void = assertEquals(m.has(s), true);
    const x: any = m.get(s);
    return x;
}
```

**Clarinet examples** for asserts and "expect" function calls
```elm
testf00 : Chain -> Map string Account -> void
testf00 chain m =
    let
        deployer : Account
        = mapgetAcc m "deployer"
        block : Block
        = fillSender deployer.address aCCList00
        |> fillTxs
        |> chain.mineBlock
        a0 : void
        = assertEquals block.height 2
        a1 : void
        = assertEquals block.receipts.length 1
        a2 : boolean
        = block.receipts[0].result.expectOk().expectBool true
    in
        globalThis.console.log block
```
generated TS
```typescript
function testf00(chain: Chain, m: Map<string, Account>): void {
    const deployer: Account = mapgetAcc(m, "deployer");
    const block: Block = chain.mineBlock(fillTxs(fillSender(deployer.address, aCCList00)));
    const a0: void = assertEquals(block.height, 2);
    const a1: void = assertEquals(block.receipts.length, 1);
    const a2: boolean = block.receipts[0].result.expectOk().expectBool(true);
    return globalThis.console.log(block);
}
```
**The test file is called from Clarinet cli after**
```elm
type alias TestOptions = {
    name: string,
    fn: any
}

testOptions0000 : TestOptions
testOptions0000 = {
        name: "test00",
        fn: testf00
    }

test0000 : void
test0000 = Clarinet.test testOptions0000
```
generated TS
```typescript
type TestOptions = {
    name: string;
    fn: any;
}

const testOptions0000: TestOptions = {
    name: "test00",
    fn: testf00
};

const test0000: void = Clarinet.test(testOptions0000);
```

## Practical conclusion

**For the examples so far**
- Everything having a number suffix like "000" or "01" is an item that could be added to a List and/or generated automatically either statically or at runtime
- The asserts **a0** to **a2** are not yet in a List and stand as an example of what would be part of an iterable structure to be approached functionally.

**Intuition expresses itself** through a raw usage of functional lingo like "List" and "application" bluntly delivering the realization that
- functional thinking has **enormous potential in this _very_ "iterable" context**.
- there is palpable human benefit to go further with this project
    - reduce Engineer stress, improve result quality
    - take almost all routine testing effort off the table and make way for more elevated tasks
    - ensure critical questions in this high-value context are answered
    - keep everything human-level, again ensure...
    - build a steppingstone for who knows what's next? :)

**First implementation** ideas are naturally more _hopeful_, _coarse_ and _frictionless_
- generate Derw "leaf" files containing "iterable" items structuring method names, argument objects, functions etc.
- have "branch" files that structure these into Lists, appropriate objects and functions
- have "trunk" and "root" files that generate the code to tie all the above together
    - read from DB, decode documents and edit leaves, branches, trunks
    - run scripts, compilation steps
    - operate in at least a two-stage manner to allow user to edit files
- final compilation, structuring and write to DB

**For the future** the Engine will go beyond generating Derw files and I have to work with design & code to:
- gain from having well structured runtime objects, build from something simple, learn from Design Patterns
- put in the effort to write more powerful Derw functions that use objects w/ adv. List structure
- efficiently handle/modify the generated files, they will be many
- get better at functional, deepen the levels of representation and nuances of what is possible

## License
This project is licensed under the terms of the AGPLv3 license.

[^1]: from epic Torvalds statement about potholes and stars :)